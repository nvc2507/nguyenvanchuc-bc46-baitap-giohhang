import React from 'react'
import ProductItem from './ProductItem'

const ProductList = (props) => {
    const {data, handlePrdDetail} = props
  return (
    <div className='row'>
        {
            data.map((item) => <ProductItem key={item.id} product ={item} handlePrdDetail={handlePrdDetail}/>)
        }
    </div>
  )
}

export default ProductList