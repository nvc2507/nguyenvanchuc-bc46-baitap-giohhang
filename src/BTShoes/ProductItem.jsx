import React from 'react'

const ProductItem = (props) => {
    const {product, handlePrdDetail} = props
  return (
    
        <div className="col-4 mt-3">
        <div className="card">
          <img src={product.image} alt="" />
          <div className="card-body" style={{width: "198px", height: '240px'}}>
                <h3>{product.name}</h3>
                <p>{product.price}$</p>

                <div>
                <button 
                    className="btn btn-outline-success mt-3"
                    style={{position: 'absolute', bottom: '10px',left:'10px'}} 
                    data-toggle="modal" data-target="#exampleModal"
                    onClick={() => handlePrdDetail(product)}>
                    Xem chi tiết
              
                </button>
                </div>
            
            
              
              
            
            </div>
        </div>
    </div>
    
  )
}

export default ProductItem