import React, { useState } from "react";
import ProductList from "./ProductList";
import data from "./data.json";

const BTShoes = () => {
  const [prdDetail, setPrdDetail] = useState(data[0]);

  const handlePrdDetail = (product) => {
    setPrdDetail(product);
  };

  return (
    <div className="container mt-5 mb-5">
      <h1 className="text-center">BTShoes</h1>
      <ProductList data={data} handlePrdDetail={handlePrdDetail} />

      {/* modal */}
      <div>
        <button
          type="button"
          className="btn btn-primary d-none"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Launch demo modal
        </button>
        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Chi Tiết Sản Phẩm
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-4">
                    <img className="img-fluid" src={prdDetail.image} alt="" />
                  </div>
                  <div className="col-8">
                    <p className="font-weight-bold">{prdDetail.name}</p>
                    <p className="mt-3">{prdDetail.description}</p>
                    <p className="font-weight-bold mt-3">{prdDetail.price}$</p>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BTShoes;
