import logo from './logo.svg';
import './App.css';
import BTShoes from './BTShoes/BTShoes';

function App() {
  return (
    <div className="container">
        <BTShoes />
    </div>
  );
}

export default App;
